import {Component, OnInit} from '@angular/core';
import {ServiceService} from "../shared/services/service.service";
import {Service} from "../shared/models/service.model";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit{

  services: Service[] = [];
  constructor( public serviceService: ServiceService) {
  }
  ngOnInit():void{
  this.serviceService.getAll().subscribe(serviceResponse => this.services = serviceResponse )
  }

  protected readonly Service = Service;
}
