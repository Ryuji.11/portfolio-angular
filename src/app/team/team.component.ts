import {Component, OnInit} from '@angular/core';
import {TeamMember} from "../shared/models/team-member.model";
import {TeamMemberService} from "../shared/services/team-member.service";

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit{

  teamMembers: TeamMember[] = [];
  constructor( public teamMemberService: TeamMemberService) {
  }
  ngOnInit():void{
    this.teamMemberService.getAll().subscribe(teamMemberResponse => this.teamMembers = teamMemberResponse )
  }

    protected readonly TeamMember = TeamMember;
}
