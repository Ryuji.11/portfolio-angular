import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Portfolio} from "../models/portfolio.model";

@Injectable({
  providedIn: 'root'
})
export class PortfolioService {

  constructor(public http: HttpClient) { }

  getAll(): Observable<Portfolio[]>{
    return this.http.get<Portfolio[]>(
      'https://angular-api.bes-webdeveloper-seraing.be/api/realisations'
    )
  }

}
