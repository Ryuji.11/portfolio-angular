import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {TeamMember} from "../models/team-member.model";

@Injectable({
  providedIn: 'root'
})
export class TeamMemberService {

  constructor(public http: HttpClient) { }
  getAll(): Observable<TeamMember[]> {
    return this.http.get<TeamMember[]>(
      'https://angular-api.bes-webdeveloper-seraing.be/api/team-members'
    );
  }
}
