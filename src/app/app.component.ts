import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = '';
  isHomePage! :boolean;
  constructor(private router: Router) {}

  ngOnInit() {

    //   permet de détecter lorsque la navigation dans l'application Angular est terminée (lorsque l'URL a été modifiée avec succès).
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // une fois l'évenement du router terminé attribue à isHomePage la valeur de la route URL en l'occurence la valeur de la route menant à la page home
    this.isHomePage = this.router.url === '/';
      }
    });
  }
}
