import {Component, OnInit} from '@angular/core';
import {Portfolio} from "../shared/models/portfolio.model";
import {PortfolioService} from "../shared/services/portfolio.service";

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit{

  portfolios: Portfolio[] = [];

  constructor( public servicePortfolio : PortfolioService) {
  }
  ngOnInit(): void {
        this.servicePortfolio.getAll().subscribe(portfolioResponse => this.portfolios = portfolioResponse)
    }
}
