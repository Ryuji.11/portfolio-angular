import { Component } from '@angular/core';
import {ContactMessage} from "../shared/models/contact-message.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ContactMessageService} from "../shared/services/contact-message.service";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent {
  contactForm: FormGroup|null = null;
  messageSent = false;

constructor(private formBuilder: FormBuilder, private contactMessageService : ContactMessageService) {
  this.initForm();
}

  initForm(){
  this.contactForm = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    message: ['', [Validators.required, Validators.minLength(30)]]
  })
  }

  sendMessage(){
  if(this.contactForm){
    const formValue = this.contactForm.value;
    const message = new ContactMessage();
      message.email = formValue['email'];
      message.date = new Date;
      message.message = formValue['message'];

      this.contactMessageService.postMessage(message).subscribe( response => {
        this.messageSent = true;
      })

  }
  }

  protected readonly ContactMessage = ContactMessage;
}
